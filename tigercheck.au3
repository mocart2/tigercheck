;include datetime lib
#include <Date.au3>

;set config ini file
$sPath_ini = @ScriptDir & "\config.ini"
;set log file path from config.ini
$logFile = IniRead($sPath_ini,"General","logFile","")

;set values from config.ini file
$alertWindowTitle = IniRead($sPath_ini,"General","alertWindowTitle","")
$searchingText = IniRead($sPath_ini,"General","searchingText","")
$logErrorValue = IniRead($sPath_ini,"General","logErrorValue","")
$logResolveValue = IniRead($sPath_ini,"General","logResolveValue","")
$timestamp = "timestamp " & _Now()
;MsgBox(0, "Info", $timestamp)


;get text from alerted window
	If WinExists($alertWindowTitle) Then
		$hWind = WinWait($alertWindowTitle)
		$msgBoxText = ControlGetText($hWind, "", "Static1")
		if $msgBoxText = $searchingText Then
		;$msgBox = MsgBox(0, "Info", $msgBoxText)
		FileOpen($logFile, 2)
		FileWrite($logFile, $timestamp & " " & $logErrorValue)
		FileClose($logFile)
		EndIf
		;$msgBox = MsgBox(0, "Info", "Alert Windows is detected!")
	Else
		;MsgBox(0, "Info", "Alert Windows is Not detected!")
		FileOpen($logFile, 2)
		FileWrite($logFile, $timestamp & " " &  $logResolveValue)
		FileClose($logFile)
	EndIf
