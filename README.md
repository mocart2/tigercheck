# tigercheck

simple utility for checking alert messages in Windows MessageBox

## using
1. copy **tigercheck.exe** and **config.ini** files to some scrips dir (For ex: C:\scripts\)
2. change **config.ini** file to your own variables (*like logfile path, searchtext etc.*)
3. setup your windows scheduler to start **C:\scripts\tigercheck.exe** with time interval which you need
4. every time, when **tigercheck.exe** will be start, it checks alert window exists, with parameters defined in **config.ini**  and compare it:

```ini
[General]
;set logfile full path
logFile='C:\Users\sudouser\Desktop\autoit\alerts.log'
;set searching alert window title
alertWindowTitle='Alert'
;set searching message test at alert window
searchingText='HW'
;set string to write at error
logErrorValue='iqconnect_error 1'
;set string to write at resolve
logResolveValue='iqconnect_error 0'
```
## Testing
You can test it by using powershell ISE command:
<img src="https://i.ibb.co/CKMtFHC/pshise.png" alt="pshise" border="0">

First of all, you must change PowerShell Default Execution Policy from restricted to unrestricted (PS must be started by Administrator):
```powershell
Set-ExecutionPolicy unrestricted
```
you can check current policy by:
```powershell
Get-ExecutionPolicy
```
then you can show test MessageBox Window
```powershell
[System.Windows.MessageBox]:Show('HW','Alert')
```
where 
* **HW** - is text of message box
* **alert** - title of message box window

app writed in AutoIt v3 scripting language, you can use source code **tigercheck.au3** for free